//
//  LoginView.h
//  MTL mPos
//
//  Created by Athiwat Thongnuan on 3/5/56 BE.
//  Copyright (c) 2556 Athiwat Thongnuan. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LoginView;
@class ViewController;
@interface LoginView : UIViewController <UITextFieldDelegate,NSXMLParserDelegate>
{
    IBOutlet UIScrollView *viewlogin;
    IBOutlet UITextField *fld_agent_number;
    IBOutlet UITextField *fld_agent_password;

    UIImageView *imageView;
    
    BOOL keyboardVisible;
	CGPoint offset;
	UITextField *activeField;
    
    
    UIEdgeInsets    _priorInset;
    BOOL            _priorInsetSaved;
    BOOL            _keyboardVisible;
    CGRect          _keyboardRect;
    CGSize          _originalContentSize;
    
    NSMutableData *webData;
	NSXMLParser *xmlParser;
    NSInteger depth;
    NSString *currentElement;
    NSMutableString *fld_result;
    NSMutableString *fld_sessionID;
    NSMutableString *fld_agent_name;
    NSMutableString *fld_agent_department;
    NSMutableString *fld_agent_position;
    NSMutableString *fld_agent_type;
    NSMutableString *fld_agent_status;
    
    IBOutlet UIActivityIndicatorView *activitying10;
    NSTimer *timer;
    IBOutlet UIButton *BloginiPad;

    
    UIImage *imageBG;
    UITapGestureRecognizer* dTap;
    
    NSMutableString *numberagents;
    NSMutableString *passagent;
    
}
@property (strong, nonatomic) IBOutlet UILabel *versionNowLogin;
@property (strong , nonatomic) UIImageView *imageView;
@property (strong,nonatomic) NSMutableString *numberagents;
@property (strong,nonatomic) NSMutableString *passagent;

@property (retain,nonatomic) ViewController *putviewlogin;
@property (strong, nonatomic) IBOutlet UIScrollView *viewlogin;
@property (strong, nonatomic) IBOutlet UITextField *fld_agent_number;
@property (strong, nonatomic) IBOutlet UITextField *fld_agent_password;


@property (nonatomic,retain) NSMutableData *webData;
@property (nonatomic,retain) NSXMLParser *xmlParser;
@property (nonatomic,retain) NSMutableString *fld_result;
@property (nonatomic,retain) NSMutableString *fld_sessionID;
@property (nonatomic,retain) NSMutableString *fld_agent_name;
@property (nonatomic,retain) NSMutableString *fld_agent_department;
@property (nonatomic,retain) NSMutableString *fld_agent_position;
@property (nonatomic,retain) NSMutableString *fld_agent_type;
@property (nonatomic,retain) NSMutableString *fld_agent_status;
@property (nonatomic,retain) NSString *elementname;
@property (strong, nonatomic) IBOutlet UIButton *BloginiPad;

- (IBAction)Blogin:(id)sender;
- (void) doubleTap : (UIGestureRecognizer*) sender;
-(NSString *) dataFilePath;
-(void) writePlist;
-(void) readPlits;

@property (strong, nonatomic) UITapGestureRecognizer* dTap;
@property (strong, nonatomic) UIImage *imageBG;
@end
