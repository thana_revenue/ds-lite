//
//  LoginView.m
//  MTL mPos
//
//  Created by Athiwat Thongnuan on 3/5/56 BE.
//  Copyright (c) 2556 Athiwat Thongnuan. All rights reserved.
//

#import "LoginView.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#define SCROLLVIEW_CONTENT_HEIGHT 1004
#define SCROLLVIEW_CONTENT_WIDTH  768
#define startActivityIndicator  [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES]
#define stopActivityIndicator  [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
@interface LoginView ()

@end

@implementation LoginView
@synthesize putviewlogin;
@synthesize viewlogin;
@synthesize fld_agent_password,fld_agent_number;
@synthesize webData,xmlParser;
@synthesize fld_result,fld_sessionID,fld_agent_name,fld_agent_department,fld_agent_position,fld_agent_type,fld_agent_status;
@synthesize elementname;
@synthesize dTap,imageBG,BloginiPad;
@synthesize numberagents;
@synthesize passagent;
@synthesize versionNowLogin;


NSString *numberagentsStr;
NSString *passagentStr;
NSString *valueInsurance;
NSInteger _numberagents;
NSInteger _passagent;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:@"LoginView" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	NSLog(@"Registering for keyboard events");
	
	// Register for the events
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector (keyboardDidShow:)
	 name: UIKeyboardWillShowNotification
	 object:nil];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self
	 selector:@selector (keyboardDidHide:)
	 name: UIKeyboardWillHideNotification
	 object:nil];
    
	
	// Setup content size
	viewlogin.contentSize = CGSizeMake(SCROLLVIEW_CONTENT_WIDTH,
                                       SCROLLVIEW_CONTENT_HEIGHT);
	//self.scrollview.frame.size.width,self.scrollview.frame.size.height
	//Initially the keyboard is hidden
	keyboardVisible = NO;
}

-(void) viewWillDisappear:(BOOL)animated {
	NSLog (@"Unregister for keyboard events");
	[[NSNotificationCenter defaultCenter]
	 removeObserver:self];
    
}

-(void) keyboardDidShow: (NSNotification *)notif {
	NSLog(@"Keyboard is visible");
	// If keyboard is visible, return
	if (keyboardVisible) {
		NSLog(@"Keyboard is already visible. Ignore notification.");
		return;
	}
	

	NSDictionary* info = [notif userInfo];
	NSValue* aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
	CGSize keyboardSize = [aValue CGRectValue].size;
	
	// Save the current location so we can restore
	// when keyboard is dismissed
	offset = viewlogin.contentOffset;
	
	// Resize the scroll view to make room for the keyboard
	CGRect viewFrame = viewlogin.frame;
	viewFrame.size.height -= keyboardSize.height;
	viewlogin.frame = viewFrame;
	
	CGRect textFieldRect = [activeField frame];
	textFieldRect.origin.y += 10;
	[viewlogin scrollRectToVisible:textFieldRect animated:YES];
	
	NSLog(@"ao fim");
	// Keyboard is now visible
	keyboardVisible = YES;
    if (keyboardVisible) {
        dTap = [[UITapGestureRecognizer alloc] initWithTarget : self  action : @selector (doubleTap:)];
        
        // [doubleTap setDelaysTouchesBegan : YES];
        dTap.numberOfTapsRequired = 1;
        dTap.numberOfTouchesRequired = 1;
        
        [self.view addGestureRecognizer : dTap];
        
    }

}

-(void) keyboardDidHide: (NSNotification *)notif {
	// Is the keyboard already shown
	if (!keyboardVisible) {
		NSLog(@"Keyboard is already hidden. Ignore notification.");
		return;
	}
	
	// Reset the frame scroll view to its original value
	viewlogin.frame = CGRectMake(0, 0, SCROLLVIEW_CONTENT_WIDTH, SCROLLVIEW_CONTENT_HEIGHT);
	
	// Reset the scrollview to previous location
	viewlogin.contentOffset = offset;
	
	// Keyboard is no longer visible
	keyboardVisible = NO;
     [self.view removeGestureRecognizer:dTap];
	
}

-(BOOL) textFieldShouldBeginEditing:(UITextField*)textField {
    activeField = textField;
    NSLog(@"textFieldShouldBeginEditing");
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"TextFieldShouldRetrun");
    activeField=nil;
	[textField resignFirstResponder];
	return YES;
}



- (void)viewDidLoad
{
    fld_agent_number.text=nil;
    fld_agent_password.text=nil;

    CGRect frameRect = fld_agent_number.frame;
    frameRect.size.height = 40;
    fld_agent_number.frame = frameRect;
    
    CGRect frameRect2 = fld_agent_password.frame;
    frameRect2.size.height = 40;
    fld_agent_password.frame = frameRect2;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *loadVersion = [defaults objectForKey:@"versionNow"];
    [versionNowLogin setText:[NSString stringWithFormat:@"Version %@",loadVersion]];
    
    
    [self addGestureRecognizersToPiece:self.BloginiPad];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fld_agent_password.secureTextEntry = YES;
   
  
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    //[[UIImage imageNamed:@"bg_ipad_UI_app_muang_thai.png"] drawInRect:self.view.bounds];
    [[UIImage imageNamed:@"bgiPad2boxLoginNew.png"] drawInRect:self.view.bounds];
    imageBG = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    viewlogin.backgroundColor = [UIColor colorWithPatternImage:imageBG];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) doubleTap : (UIGestureRecognizer*) sender
{
    NSLog (@"tap Do operations here...");
    [activeField resignFirstResponder];
    [fld_agent_number resignFirstResponder];
    [fld_agent_password resignFirstResponder];

   
}
- (void)addGestureRecognizersToPiece:(UIView *)piece
{
    
    UIRotationGestureRecognizer *Buttonlogin = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(Blogin:)];
    [piece addGestureRecognizer:Buttonlogin];
    
    
    
}
- (void)viewDidUnload {
    viewlogin = nil;
    fld_agent_number = nil;
    fld_agent_password = nil;
    [self setViewlogin:nil];
    [self setFld_agent_number:nil];
    [self setFld_agent_password:nil];
 
    [self setBloginiPad:nil];
    
    activitying10 = nil;
    BloginiPad = nil;

    [self setVersionNowLogin:nil];
    [super viewDidUnload];
}
-(void) serviceSoap
{
    numberagentsStr= fld_agent_number.text;
    passagentStr = fld_agent_password.text;
    numberagents = [[NSMutableString alloc] init];
    passagent =[[NSMutableString alloc] init];

    
    int j = [numberagentsStr length];
    for (int i=0; i<j; i++) {
        if ([numberagentsStr characterAtIndex:i] >=48 && [numberagentsStr characterAtIndex:i] <=59) {
            [numberagents appendFormat:@"%c",[numberagentsStr characterAtIndex:i]];
        }
    }
    int k = [passagentStr length];
    for (int i=0; i<k; i++) {
        if ([passagentStr characterAtIndex:i] >=48 && [passagentStr characterAtIndex:i] <=59) {
            [passagent appendFormat:@"%c",[passagentStr characterAtIndex:i]];
        }
    }

    
    if (([fld_agent_number.text length]==0)) {
		
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message Error" message:@"กรุณากรอกเลขที่ตัวแทน" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok",nil];
		[alert show];
		//[alert release];
	}else if (([fld_agent_password.text length]==0))
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Message Error" message:@"กรุณากรอกรหัสผ่าน" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok",nil];
		[alert show];
    }
	else {
		
		//[NumberAgents resignFirstResponder];
		NSString *soapFormat = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
								"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
								"<soap:Body>\n"
								"<CheckAgentAuthen xmlns=\"http://muangthai.co.th/\">\n"
								"<fld_agent_number>%@</fld_agent_number>\n"
                                "<fld_agent_password>%@</fld_agent_password>\n"
                                "<fld_partner_username>5fef0966a24941da91b5315faa57ae01</fld_partner_username>\n"
                                "<fld_partner_password>22283283</fld_partner_password>\n"
                                "<fld_partner_IPAddress></fld_partner_IPAddress>\n"
								"</CheckAgentAuthen>\n"
								"</soap:Body>\n"
								"</soap:Envelope>\n",fld_agent_number.text,fld_agent_password.text];
		
		
		//NSLog(@"The request format is %@",soapFormat);
		
		NSURL *locationOfWebService = [NSURL URLWithString:@"http://www.muangthai.co.th/MTLWebService/WS_For_MTLmPOS.asmx"];
		
		//NSLog(@"web url = %@",locationOfWebService);
		
		NSMutableURLRequest *theRequest = [[NSMutableURLRequest alloc]initWithURL:locationOfWebService];
		
		NSString *msgLength = [NSString stringWithFormat:@"%d",[soapFormat length]];
		
		[theRequest addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
		[theRequest addValue:@"http://muangthai.co.th/CheckAgentAuthen" forHTTPHeaderField:@"SOAPAction"];
		[theRequest addValue:msgLength forHTTPHeaderField:@"Content-Length"];
		[theRequest setHTTPMethod:@"POST"];
		//the below encoding is used to send data over the net
		[theRequest setHTTPBody:[soapFormat dataUsingEncoding:NSUTF8StringEncoding]];
		
		
		NSURLConnection *connect = [[NSURLConnection alloc]initWithRequest:theRequest delegate:self];
		
		if (connect) {
			webData = [[NSMutableData alloc]init];
            startActivityIndicator;
            [self ActivityIndicator_view];
		}
		else {
			NSLog(@"No Connection established");
		}
		
	}
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[webData setLength: 0];
    
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	[webData appendData:data];
    
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"ERROR with theConenction");
    UIAlertView *alert1 = [[UIAlertView alloc]
                           initWithTitle:@"Message Error"
                           message:@"ไม่ได้ต่ออินเตอร์เน็ต"
                           delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil];
    [alert1 show];
    
	//[connection release];
	//[webData release];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"DONE. Received Bytes: %d", [webData length]);
	NSString *theXML = [[NSString alloc] initWithBytes: [webData mutableBytes] length:[webData length] encoding:NSUTF8StringEncoding];
	NSLog(@"the XML is : %@",theXML);
	
	xmlParser = [[NSXMLParser alloc]initWithData:webData];
	[xmlParser setDelegate: self];
	//[xmlParser setShouldResolveExternalEntities: YES];
	[xmlParser parse];

    
    
    stopActivityIndicator;
    [self enableActivityIndicator_view];
    //[activitying10 stopAnimating];
}


//xml delegates

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    
    currentElement = [elementName copy];
    
    
    if ([currentElement isEqualToString:@"CheckAgentAuthenResult"])
    {
        ++depth;
        [self showCurrentDepth];
    }
    else if ([currentElement isEqualToString:@"fld_result"])
    {
      
        fld_result = [[NSMutableString alloc] init];
    }
    else if ([currentElement isEqualToString:@"fld_sessionID"])
    {
        
        fld_sessionID = [[NSMutableString alloc] init];
    }
    else if ([currentElement isEqualToString:@"fld_agent_name"])
    {
        
        fld_agent_name = [[NSMutableString alloc] init];
    }else if ([currentElement isEqualToString:@"fld_agent_department"])
    {
        
        fld_agent_department = [[NSMutableString alloc] init];
    }
    else if ([currentElement isEqualToString:@"fld_agent_position"])
    {
        
        fld_agent_position = [[NSMutableString alloc] init];
    }
    else if ([currentElement isEqualToString:@"fld_agent_type"])
    {
        
        fld_agent_type = [[NSMutableString alloc] init];
    }else if ([currentElement isEqualToString:@"fld_agent_status"])
    {
       
        fld_agent_status = [[NSMutableString alloc] init];
    }
    
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    ////////////////////////////// function 1
    if ([currentElement isEqualToString:@"fld_result"])
    {
        [fld_result appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_sessionID"])
    {
        [fld_sessionID appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_agent_name"])
    {
        [fld_agent_name appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_agent_department"])
    {
        [fld_agent_department appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_agent_position"])
    {
        [fld_agent_position appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_agent_type"])
    {
        [fld_agent_type appendString:string];
    }else  if ([currentElement isEqualToString:@"fld_agent_status"])
    {
        [fld_agent_status appendString:string];
    }
    
    
}
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
	
    /////////////////////////////////// function 1
    if ([elementName isEqualToString:@"CheckAgentAuthenResult"])
    {
        --depth;
        [self showCurrentDepth];
    }
    else if ([elementName isEqualToString:@"fld_result"])
    {
        if (depth == 1)
        {
            
        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_sessionID"])
    {
        if (depth == 1)
        {
            
        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_agent_name"])
    {
        if (depth == 1)
        {
           
        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_agent_department"])
    {
        if (depth == 1)
        {
            
        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_agent_position"])
    {
        if (depth == 1)
        {
            
        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_agent_type"])
    {
        if (depth == 1)
        {

        }
        else
        {
            
        }
    }else if ([elementName isEqualToString:@"fld_agent_status"])
    {
        if (depth == 1)
        {
         
        }
        else
        {
           
        }
    }
    
}
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
{
    
}
-(void) parserDidStartDocument:(NSXMLParser *)parser
{
    
    NSLog(@"Document started", nil);
    depth = 0;
    currentElement = nil;
}
-(void) parserDidEndDocument:(NSXMLParser *)parser
{

    if ([fld_result isEqual: @"passed"]) {
        //ViewController *detailVC = [[ViewController alloc] init];
        
        
        //[self writePlist];
        NSMutableArray *anArray1 = [[NSMutableArray alloc] init];
        [anArray1 addObject:fld_agent_number.text];
        [anArray1 addObject:fld_agent_password.text];
        [anArray1 addObject:fld_result];
        
        
        ///ตัดช่องว่างชื่อตัวแทน
        NSArray* wordsmessage = [fld_agent_name componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceCharacterSet]];
        NSString* strfld_agent_name = [wordsmessage componentsJoinedByString:@""];
        strfld_agent_name = [strfld_agent_name stringByReplacingOccurrencesOfString:@""
                                                                         withString:@""];
        [anArray1 addObject:fld_agent_name];
        
        
        ///fld_agent_status
        if (fld_agent_status==nil) {
            fld_agent_status=[NSMutableString stringWithString:@""];
            [anArray1 addObject:fld_agent_status];
            NSLog(@"agent status is NULL");
        }else{
            [anArray1 addObject:fld_agent_status];
            NSLog(@"agent status is not null");
        }
        
        
        [anArray1 writeToFile:[self dataFilePath] atomically:YES];
        
        putviewlogin = [[ViewController alloc] init];
        [self presentModalViewController:putviewlogin animated:YES];
        
        
        
        
    } else{
       // [self PasswordAlert];
        
        NSString *str = fld_result;
        NSString *newStr;
        
        newStr = [str substringWithRange:NSMakeRange(8, str.length -8)];
        NSLog(@"%@", newStr);
        
        [self PasswordAlert:newStr];
    }
    
    NSLog(@"Document finished", nil);
    
}
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"Error: %@", [parseError localizedDescription]);
}
- (void)showCurrentDepth
{
    NSLog(@"Current depth: %d", depth);
}
-(void) PasswordAlert:(NSString *)st
{
    UIAlertView *alert1 = [[UIAlertView alloc]
                           initWithTitle:@"Message Error"
                         //  message:@"เลขที่ตัวแทนหรือรหัสผ่านไม่ถูกต้อง"
                           message:st
                           delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil];
    [alert1 show];
}

- (IBAction)Blogin:(id)sender {
    [self serviceSoap];
}
-(NSString *) dataFilePath
{
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); NSString *documentDirectory = [path objectAtIndex:0];
    return [documentDirectory stringByAppendingPathComponent:@"AgentNandAgentP.plist"];
}

- (void)writePlist
{
    NSMutableArray *anArray = [[NSMutableArray alloc] init];
    [anArray addObject:fld_agent_number.text];
    [anArray addObject:fld_agent_password.text];

    [anArray writeToFile:[self dataFilePath] atomically:YES];
}

-(void)readPlist
{
    NSString *filePath1 = [self dataFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath1])
    {
        NSArray *array1 = [[NSArray alloc] initWithContentsOfFile:filePath1];

        NSString *strCheck=array1[2];
        if ([strCheck isEqual:@"passed"]) {
            NSLog(@"checking");
            putviewlogin = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
            [self presentModalViewController:putviewlogin animated:YES];
            
        }else {
            NSLog(@"Other!!!!");
        }
        
        //NSLog(@"%@\n", filePath); //[array release];
    }

}
-(void) ActivityIndicator_view{
    [activitying10 startAnimating];
    CGRect myImageRect = CGRectMake(0, 0, 2048, 1536);
    imageView = [[UIImageView alloc] initWithFrame:myImageRect];
    [imageView setImage:[UIImage imageNamed:@"backiPad.png"]];
    [self.view addSubview:imageView];

    fld_agent_number.userInteractionEnabled=NO;
    fld_agent_password.userInteractionEnabled=NO;
    BloginiPad.userInteractionEnabled=NO;
    
}
-(void) enableActivityIndicator_view{
    [activitying10 stopAnimating];
    [imageView removeFromSuperview];
    fld_agent_number.userInteractionEnabled=YES;
    fld_agent_password.userInteractionEnabled=YES;
    BloginiPad.userInteractionEnabled=YES;
    
}

@end
